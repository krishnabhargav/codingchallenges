﻿module Permutations

    let slow (ns: 'a list)  = 
        let rotate (x::xs) = xs @ [x]
        let rotates xs = 
                xs 
                |> List.fold 
                    (fun (s,p) _ -> 
                            let rotated = rotate p
                            rotated :: s, rotated)
                    ([],xs)
                |> fst
        let rec innerPerm items =
                let generatePerms xs = 
                    let head, tail = xs |> List.head, xs |> List.tail
                    tail 
                    |> innerPerm 
                    |> List.map(fun f -> head :: f)
                match items with
                | [] -> []
                | [x] -> [[x]]
                | xs ->
                    let rs = rotates xs
                    rs
                    |> List.collect generatePerms
                    
        innerPerm ns

    let perm2 (ns: 'a list)  = 
        let injectAll h l = 
            [0..l |> List.length]
            |> List.map
                (fun i -> 
                    let first,rest = l |> List.splitAt i
                    first @ (h :: rest)
                )

            
        let rec innerPerm items =
                let result = 
                    match items with
                    | [] -> []
                    | [x] -> [[x]]
                    | [x;y] -> [[x;y];[y;x]]
                    | head::tail ->
                        innerPerm tail 
                        |> Seq.collect (injectAll head)
                        |> List.ofSeq
                result

        innerPerm ns










﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.

[<EntryPoint>]
let main argv = 
    let s = Permutations.perm2 [1..3]
    printfn "%A" (s |> List.length)
    System.Console.ReadLine() |> ignore
    0